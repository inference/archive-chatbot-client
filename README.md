# README #

Chatbot client

### What is this repository for? ###

* Quick summary
* Version 1.0
* Basic chatbot client to test Studio chatbot tasks

### How do I get set up? ###

* Clone the repository to access the files. Use the html file as it is or modify it to suite your application requirements.
* To connect to a Studio chatbot follow the development guildelines available in the Resource Center.
* Replace the logo with the location of your logo to customise the appearance.

### Who do I talk to? ###

* Contact support for more information